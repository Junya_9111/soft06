class ProductController < ApplicationController
  def index
    @products = Product.all
    @cartitem = Cartitem.new
  end
  
  def new
    @product = Product.new
  end
  
  def create
    product = Product.new(name:params[:product][:name], price:params[:product][:price])
    product.save
    redirect_to product_index_path
  end
  
  def destroy
    product = Product.find(params[:id])
    product.destroy
    redirect_to product_index_path
  end
end
