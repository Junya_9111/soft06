class CartitemsController < ApplicationController
  def create
    cartitem = Cartitem.new(qty:params[:qty], product_id:params[:product_id], cart_id:params[:cart_id])
    cartitem.save
    redirect_to product_index_path
  end
  
  def destroy
    cartitem = Cartitem.find(params[:id])
    cartitem.destroy
    redirect_to cart_show_path
  end
end
