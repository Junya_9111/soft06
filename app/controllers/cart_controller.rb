class CartController < ApplicationController
  def show
    @cartitems = Cartitem.all
  
    @total_price = 0
    @cartitems.each do |cartitems|
      @total_price += cartitems.product.price * cartitems.qty
    end
  end
end

